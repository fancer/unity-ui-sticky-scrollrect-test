﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(ScrollStickyElement))]
[System.Serializable]
public class StickyElementId : MonoBehaviour {

	/* Need some Id to identify the current element */
	[SerializeField]
	private int m_Id;
	public int Id {
		set {
			m_Id = value;
		}
		get { return m_Id; }
	}
}
