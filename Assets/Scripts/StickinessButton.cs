﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
[System.Serializable]
public class StickinessButton : MonoBehaviour {

	// Sticky Scroll View object
	[SerializeField]
	private ScrollRect m_scrollRect;

	// Image component being used to set the button background
	[SerializeField]
	private Image m_togglerImage;

	// Selected spot sprite
	[SerializeField]
	private Sprite m_enabledSprite;

	// Enabled spot sprite
	[SerializeField]
	private Sprite m_disabledSprite;

	// Initialize the current state of the toggle
	void Start () {
		// Initialize the button if the specified sprites in compiance with the 
		// current stickiness state
		if (m_scrollRect.StickinessIsEffectivelyOn)
			m_togglerImage.sprite = m_enabledSprite;
		else
			m_togglerImage.sprite = m_disabledSprite;
	}

	// Interface function to set the enabled image on the Button
	public void SetEnabledImage() {
		m_togglerImage.sprite = m_enabledSprite;
	}

	// Interface function to set the disable image on the Button
	public void SetDisabledImage() {
		m_togglerImage.sprite = m_disabledSprite;
	}
	
	// Button backgournd toggler
	public void ToggleStickinessState() {
		if (m_scrollRect.stickiness) {
			m_scrollRect.stickiness = false;
			// The stikiness is disable in any case
			SetDisabledImage();
		} else {
			m_scrollRect.stickiness = true;
			// Toggle the Enabled image only if the stickiness has been really enabled
			if (m_scrollRect.StickinessIsEffectivelyOn)
				SetEnabledImage();
		}
	}
}
