﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class StickyCallback : MonoBehaviour {

	// Sticky Scroll View object
	[SerializeField]
	private ScrollRect m_scrollRect;

	// Selected spot sprite
	[SerializeField]
	private Sprite m_selectedSpot;

	// Unselected spot sprite
	[SerializeField]
	private Sprite m_unselectedSpot;

	// Array of images to change the highlights
	[SerializeField]
	private Image[] m_Images;

	// Array of images to change the highlights
	[SerializeField]
	private ScrollStickyElement[] m_StickyElements;

	// Stickiness button to toggle the image if the stickiness is off
	[SerializeField]
	private StickinessButton m_stickinessButton;

	// Callback method invoked when a new sticky element is reached. Here is the events
	// disposition:
	// Previous    Current
	//  y or x     y or x
	// where y - the object is set, x - the object is null.
	// With respect to that table, the event callback disposition can be as follows:
	// 1) The movement has just started since the object is enabled:
	//    xy - the content is moving towards a new sticky object
	// 2) The movement is initiated while there has been an object the content is stuck to
	//    yy - the content is moving towards a new sticky element.
	// 3) For some reason there is no any sticky element available to stick the content to:
	//    yx - either there is no any reachable sticky element or the stickiness is effectively off
	public void NewButtonReached(ScrollStickyElement prev, ScrollStickyElement cur) {
		int previd = null != prev ? prev.GetComponent<StickyElementId>().Id : -1;
		int curid = null != cur ? cur.GetComponent<StickyElementId>().Id : -1;

		if (null != prev) {
			m_Images [previd].sprite = m_unselectedSpot;
		}
		if (null != cur) {
			m_Images [curid].sprite = m_selectedSpot;
			// Set the enabdled image to the button since the stickiness is enabled
			m_stickinessButton.SetEnabledImage();
		} else {
			// Set the disable image to the button since the stickiness must have been disabled
			m_stickinessButton.SetDisabledImage();
		}

		//Debug.Log("Hello target element reached callback previd " + previd + ", curid " + curid);
	}

	// Mirror button callback function
	public void PushedGotoButton(int id) {
		bool sts;
		sts = m_scrollRect.GotoStickyElement(m_StickyElements[id]);
		if (!sts) {
			Debug.Log("Couldn't move the content to the sticky point with id " + id);
		}
	}
}
