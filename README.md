# README #

This is the repository of test application for Sticky ScrollRect class of Unity UI subsystem. The source code of the update UI is over here:
https://bitbucket.org/fancer/unity-ui-sticky-scrollrect/src

Additionally built libraries can be found in the attached UnityUI_5.3.4.zip archive, which password is: sticky

### How do I get set up? ###

See Unity UI repository readme file for details:
https://bitbucket.org/Unity-Technologies/ui